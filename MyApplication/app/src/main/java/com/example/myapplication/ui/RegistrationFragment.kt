package com.example.myapplication.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import com.example.myapplication.R

class RegistrationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_registration, container, false)

        view.findViewById<Button>(R.id.button_login).setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_navigation_registration_to_navigation_login)
        }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() = RegistrationFragment()
    }
}